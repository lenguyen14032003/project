package dev.ltn.demo;


import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoServiceIml implements DemoService {
    @Override
    public List<String> getTags() {
        return List.of("tag1", "tag2");
    }
}
