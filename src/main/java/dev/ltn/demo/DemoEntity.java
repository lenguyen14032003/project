package dev.ltn.demo;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DemoEntity {
    private Long id;
    private String name;
    private List<String> tags;
}
