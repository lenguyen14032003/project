package dev.ltn.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/demo") // localhost:8080/api/demo
@Slf4j
@RequiredArgsConstructor
public class DemoController {

    private final DemoService demoService;

    @GetMapping
    public DemoEntity getDemo() {
        log.info("Get/api/demo");
        return DemoEntity.builder()
                .id(1L)
                .name("Nguyen")
                .tags(demoService.getTags()).
                build();
    }
}
