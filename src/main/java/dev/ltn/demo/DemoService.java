package dev.ltn.demo;

import java.util.List;

public interface DemoService {
    List<String> getTags();
}
